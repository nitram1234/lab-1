
#!/bin/bash

clear
echo $1

if [[ $# == "0" ]]; then 
echo "Se requiere de un directorio para listar."
exit 1

else
cd /home/$USER/$1
ls -l |  awk '{print $5, $9}'|sort -n | nl

fi